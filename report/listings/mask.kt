package io.github.mreditor.computergraphics

import java.awt.image.BufferedImage
import kotlin.random.Random
import kotlin.system.measureTimeMillis

fun maskCount(): Unit = MainWindow.run {
    var count = 0
    val time = measureTimeMillis {
        withStatus("counting with abc-mask") {
            sourceImage?.let {
                val labels = Array(it.width) { _ -> Array(it.height) { 0 } }
                val dsu = Dsu()
                val c = doMask(labels) { x, y ->
                    labels[x][y] = labels[x - 1][y]
                    dsu[labels[x - 1][y]] = labels[x][y - 1]
                }
                count = c - dsu.size
            }
        }
    }
    statusLabel.text = "$count connected areas ($time ms with mask)"
}

fun maskFill(): Unit = MainWindow.run {
    val time = measureTimeMillis {
        val random = Random(0)
        withStatus("filling with abc-mask") {
            sourceImage = sourceImage?.let {
                val labels = Array(it.width) { _ -> Array(it.height) { 0 } }
                val dsu = Dsu()
                val count = doMask(labels) { x, y ->
                    labels[x][y] = labels[x - 1][y]
                    dsu[labels[x - 1][y]] = labels[x][y - 1]
                }
                val palette = Array(count + 1) { random.nextInt(0xFFFFFF) }
                BufferedImage(it.width, it.height, BufferedImage.TYPE_INT_RGB).apply {
                    for (x in 0 until it.width) for (y in 0 until it.height) {
                        setRGB(x, y, palette[dsu[labels[x][y]]])
                    }
                }
            }
        }
    }
    statusLabel.text = "filled ($time ms with bfs)"
}

fun noDsuMaskFill(leftFill: Boolean): Unit = MainWindow.run {
    val time = measureTimeMillis {
        val random = Random(0)
        withStatus("filling with abc-mask (no dsu)") {
            sourceImage = sourceImage?.let {
                val labels = Array(it.width) { _ -> Array(it.height) { 0 } }
                val count = doMask(labels) { x, y ->
                    if (leftFill) labels[x][y] = labels[x - 1][y]
                    else labels[x][y] = labels[x][y - 1]
                }
                val palette = Array(count + 1) { random.nextInt(0xFFFFFF) }
                BufferedImage(it.width, it.height, BufferedImage.TYPE_INT_RGB).apply {
                    for (x in 0 until it.width) for (y in 0 until it.height) {
                        setRGB(x, y, palette[labels[x][y]])
                    }
                }
            }
        }
    }
    statusLabel.text = "stroked ($time ms with bfs)"
}

private inline fun MainWindow.doMask(labels: Array<Array<Int>>, pointReached: (Int, Int) -> Unit): Int {
    var currentLabel = 0
    sourceImage?.let {
        for (x in 0 until it.width) for (y in 0 until it.height) {
            val currentColor = it.getRGB(x, y)
            val leftMatch = x > 0 && it.getRGB(x - 1, y) == currentColor
            val topMatch = y > 0 && it.getRGB(x, y - 1) == currentColor
            when {
                leftMatch && topMatch -> pointReached(x, y)
                leftMatch && !topMatch -> labels[x][y] = labels[x - 1][y]
                !leftMatch && topMatch -> labels[x][y] = labels[x][y - 1]
                !leftMatch && !topMatch -> labels[x][y] = ++currentLabel
            }
        }
    }
    return currentLabel
}
