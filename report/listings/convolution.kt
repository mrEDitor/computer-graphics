package io.github.mreditor.computergraphics

import java.awt.Dimension
import java.awt.image.BufferedImage
import javax.swing.*
import kotlin.math.PI
import kotlin.math.atan
import kotlin.math.roundToInt
import kotlin.math.sqrt

const val GAUSSIAN_7 = """
    0.00000067 	0.00002292 	0.00019117 	0.00038771 	0.00019117 	0.00002292 	0.00000067
    0.00002292 	0.00078633 	0.00655965 	0.01330373 	0.00655965 	0.00078633 	0.00002292
    0.00019117 	0.00655965 	0.05472157 	0.11098164 	0.05472157 	0.00655965 	0.00019117
    0.00038771 	0.01330373 	0.11098164 	0.22508352 	0.11098164 	0.01330373 	0.00038771
    0.00019117 	0.00655965 	0.05472157 	0.11098164 	0.05472157 	0.00655965 	0.00019117
    0.00002292 	0.00078633 	0.00655965 	0.01330373 	0.00655965 	0.00078633 	0.00002292
    0.00000067 	0.00002292 	0.00019117 	0.00038771 	0.00019117 	0.00002292 	0.00000067 
    """

const val SOBELX_3 = """
    -1 0 +1
    -2 0 +2
    -1 0 +1
    """

const val SOBELY_3 = """
    -1 -2 -1
    00 00 00
    +1 +2 +1
    """

fun sobelGradientMagnitude() =
    MainWindow.runWithStatus("Applying convolution", "convolution applied") {
        sourceImage?.let {
            val imageX = applyConvolution(SOBELX_3)!!
            val imageY = applyConvolution(SOBELY_3)!!
            sourceImage = BufferedImage(it.width, it.height, BufferedImage.TYPE_INT_RGB).apply {
                fun Int.unpackSq() = ((2 * this - 0xFF) * (2 * this - 0xFF)).toDouble()
                for (x in 0 until it.width) for (y in 0 until it.height) {
                    val colorX = imageX[x][y]
                    val colorY = imageY[x][y]
                    setRGB(
                        x, y, rgb(
                            sqrt((colorX.red().unpackSq() + colorY.red().unpackSq()) / 2).toInt(),
                            sqrt((colorX.green().unpackSq() + colorY.green().unpackSq()) / 2).toInt(),
                            sqrt((colorX.blue().unpackSq() + colorY.blue().unpackSq()) / 2).toInt()
                        )
                    )
                }
            }
        }
    }

fun sobelGradientOrientation() =
    MainWindow.runWithStatus("Applying convolution", "convolution applied") {
        sourceImage?.let {
            val imageX = applyConvolution(SOBELX_3)!!
            val imageY = applyConvolution(SOBELY_3)!!
            sourceImage = BufferedImage(it.width, it.height, BufferedImage.TYPE_INT_RGB).apply {
                fun Int.unpack() = (2 * this - 0xFF).toDouble()
                fun Double.fromAtan() = ((this + PI / 2) * 255 / PI).toInt()
                for (x in 0 until it.width) for (y in 0 until it.height) {
                    val colorX = imageX[x][y]
                    val colorY = imageY[x][y]
                    setRGB(
                        x, y, rgb(
                            atan(colorY.red().unpack() / colorX.red().unpack()).fromAtan(),
                            atan(colorY.green().unpack() / colorX.green().unpack()).fromAtan(),
                            atan(colorY.blue().unpack() / colorX.blue().unpack()).fromAtan()
                        )
                    )
                }
            }
        }
    }

fun showConvolutionTool(note: String? = null, preset: String? = null) = MainWindow.run {
    val a = JTextArea(preset ?: "0 0 0\n0 1 0\n0 0 0", 10, 10)
    JFrame("Convolution").apply {
        add(Box.createVerticalBox().apply {
            add(JTextArea(note ?: "you can type note here"))
            add(Box.createRigidArea(Dimension(0, 10)))
            add(JPanel().apply {
                add(a)
                add(JButton("Apply").apply {
                    addActionListener {
                        withStatus("Applying convolution", "convolution applied") {
                            sourceImage = applyConvolution(a.text)?.let {
                                val height = it.first().size
                                val width = it.size
                                BufferedImage(width, height, BufferedImage.TYPE_INT_RGB).apply {
                                    for (x in 0 until width) for (y in 0 until height) {
                                        setRGB(x, y, it[x][y])
                                    }
                                }
                            }
                        }
                    }
                })
            })
        })
        pack()
        isVisible = true
    }
}

private fun MainWindow.applyConvolution(a: String): Array<IntArray>? {
    fun trimToDouble(v: String) = v.trim().toDoubleOrNull()
    val aValue = a.trim()
        .split("\n")
        .map { it.split(" ").mapNotNull(::trimToDouble) }
        .filter { it.isNotEmpty() }
    val aNegative = aValue.any { it.any { it < 0 } }
    if ((aValue.size and 1) != 1 || aValue.any { (it.size and 1) != 1 }) {
        JOptionPane.showMessageDialog(this, "The kernel must have odd sides")
        return null
    }
    if (aValue.any { it.size != aValue.first().size }) {
        JOptionPane.showMessageDialog(this, "The kernel must been rectangle")
        return null
    }
    return sourceImage?.let {
        Array(it.width) { _ -> IntArray(it.height) }.apply {
            for (x in -aValue.first().size / 2 until it.width - aValue.first().size / 2) {
                for (y in -aValue.size / 2 until it.height - aValue.size / 2) {
                    var sumRed = 0.0
                    var sumGreen = 0.0
                    var sumBlue = 0.0
                    for (j in aValue.indices) for (i in aValue[j].indices) {
                        if (x + i in 0 until it.width && y + j in 0 until it.height) {
                            sumRed += aValue[j][i] * it.getRGB(x + i, y + j).red()
                            sumGreen += aValue[j][i] * it.getRGB(x + i, y + j).green()
                            sumBlue += aValue[j][i] * it.getRGB(x + i, y + j).blue()
                        }
                    }
                    this[x + aValue.first().size / 2][y + aValue.size / 2] =
                        if (aNegative) rgb(
                            (sumRed.roundToInt() + 0xFF) / 2,
                            (sumGreen.roundToInt() + 0xFF) / 2,
                            (sumBlue.roundToInt() + 0xFF) / 2
                        )
                        else rgb(sumRed.roundToInt(), sumGreen.roundToInt(), sumBlue.roundToInt())
                }
            }
        }
    }
}
