package io.github.mreditor.computergraphics

import java.awt.image.BufferedImage
import java.util.*
import kotlin.random.Random
import kotlin.system.measureTimeMillis

fun bfsCount(): Unit = MainWindow.run {
    var count = 0
    val time = measureTimeMillis {
        withStatus("counting with breadth-first search") {
            count = doBfs { _, _, _ -> Unit }
        }
    }
    statusLabel.text = "$count connected areas ($time ms with bfs)"
}

fun bfsFill(): Unit = MainWindow.run {
    val time = measureTimeMillis {
        val palette = mutableMapOf<Int, Int>()
        val random = Random(0)
        withStatus("filling with breadth-first search") {
            sourceImage = sourceImage?.let {
                BufferedImage(it.width, it.height, BufferedImage.TYPE_INT_RGB).apply {
                    doBfs { x, y, c -> setRGB(x, y, palette.getOrPut(c) { random.nextInt(0xFFFFFF) }) }
                }
            }
        }
    }
    statusLabel.text = "filled ($time ms with bfs)"
}

private inline fun MainWindow.doBfs(pointReached: (Int, Int, Int) -> Unit): Int {
    var currentColor = 0
    sourceImage?.let {
        val used = Array(it.width) { _ -> Array(it.height) { false } }
        val queue = ArrayDeque<Pair<Int, Int>>()
        val xRange = 0 until it.width
        val yRange = 0 until it.height
        for (x in xRange) for (y in yRange) {
            if (!used[x][y]) {
                used[x][y] = true
                queue.addLast(Pair(x, y))
                ++currentColor
            }
            while (queue.isNotEmpty()) {
                val xy = queue.pop()
                val c = it.getRGB(xy.first, xy.second)
                val bank = arrayOf(0, 0, -1, +1, 0, 0)
                for (i in 0..3) {
                    val xx = xy.first + bank[i]
                    val yy = xy.second + bank[i + 2]
                    if (xx in xRange && yy in yRange && c == it.getRGB(xx, yy)) {
                        if (!used[xx][yy]) {
                            used[xx][yy] = true
                            queue.addLast(Pair(xx, yy))
                        }
                    }
                }
                pointReached(xy.first, xy.second, currentColor)
            }
        }
    }
    return currentColor
}
