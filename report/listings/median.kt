package io.github.mreditor.computergraphics

import java.awt.image.BufferedImage
import java.util.*
import javax.swing.JOptionPane

fun medianFilter() =
    MainWindow.runWithStatus("Applying median filter", "median filter applied") {
        val filterSize = JOptionPane
            .showInputDialog(this, "Median filter side", "5")
            .toInt()
        sourceImage = sourceImage?.let {
            BufferedImage(it.width, it.height, BufferedImage.TYPE_INT_RGB).apply {
                for (x in filterSize / 2 until it.width - filterSize / 2) {
                    for (y in filterSize / 2 until it.height - filterSize / 2) {
                        val colorArrayRed = IntArray(filterSize * filterSize)
                        val colorArrayGreen = IntArray(filterSize * filterSize)
                        val colorArrayBlue = IntArray(filterSize * filterSize)
                        for (fx in 0 until filterSize) for (fy in 0 until filterSize) {
                            val i = fx * filterSize + fy
                            val color = it.getRGB(x + fx - filterSize / 2, y + fy - filterSize / 2)
                            colorArrayRed[i] = color.red()
                            colorArrayGreen[i] = color.green()
                            colorArrayBlue[i] = color.blue()
                        }
                        Arrays.sort(colorArrayBlue)
                        Arrays.sort(colorArrayGreen)
                        Arrays.sort(colorArrayRed)
                        setRGB(
                            x, y,
                            rgb(
                                colorArrayRed[filterSize * filterSize / 2],
                                colorArrayGreen[filterSize * filterSize / 2],
                                colorArrayBlue[filterSize * filterSize / 2]
                            )
                        )
                    }
                }
            }
        }
    }