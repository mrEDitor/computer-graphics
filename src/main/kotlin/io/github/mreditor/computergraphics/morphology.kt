package io.github.mreditor.computergraphics

import java.awt.Dimension
import java.awt.image.BufferedImage
import javax.swing.*

fun showMorphologyTool(note: String? = null) = MainWindow.run {
    val a = JTextArea("0 0 0\n0 1 0\n0 0 0", 10, 10)
    val b = JTextArea("0 1 0\n1 1 1\n0 1 0", 10, 10)
    JFrame("Morphology").apply {
        add(Box.createVerticalBox().apply {
            add(JTextArea(note ?: "you can type note here"))
            add(Box.createRigidArea(Dimension(0, 10)))
            add(a)
            add(Box.createRigidArea(Dimension(0, 10)))
            add(b)
            add(JButton("a -> b").apply {
                addActionListener { applyMorphology(a.text, b.text) }
            })
            add(JButton("a <- b").apply {
                addActionListener { applyMorphology(b.text, a.text) }
            })
        })
        pack()
        isVisible = true
    }
}

private fun MainWindow.applyMorphology(a: String, b: String) =
    withStatus("Applying morphology", "morphology applied") {
        fun toBoolean(v: String) = when (v.trim()) {
            "1" -> true
            "0" -> false
            "" -> null
            else -> JOptionPane
                .showMessageDialog(this, "`$v`: Only 0 and 1 allowed as morphology elements")
                .let { null }
        }

        val aValue = a.trim()
            .split("\n")
            .map { it.split(" ").mapNotNull(::toBoolean) }
            .filter { it.isNotEmpty() }
        val bValue = b.trim()
            .split("\n")
            .map { it.split(" ").mapNotNull(::toBoolean) }
            .filter { it.isNotEmpty() }
        if (
            aValue.size != bValue.size
            || aValue.any { it.size != bValue.first().size }
            || bValue.any { it.size != aValue.first().size }
        ) {
            JOptionPane.showMessageDialog(this, "A and B masks must have same size")
            return@withStatus
        }
        sourceImage = sourceImage?.let {
            BufferedImage(it.width, it.height, BufferedImage.TYPE_INT_RGB).apply {
                for (x in 0 until it.width) for (y in 0 until it.height) {
                    setRGB(x, y, BLACK)
                }
                for (x in -aValue.first().size until aValue.first().size + it.width) {
                    for (y in -aValue.size until aValue.size + it.height) {
                        var match = true
                        for (j in aValue.indices) for (i in aValue[j].indices) {
                            if (aValue[j][i] && x + i in 0 until it.width && y + j in 0 until it.height) {
                                match = match &&
                                        it.getRGB(x + i, y + j).grayscale(0.3, 0.4, 0.3) > WHITE / 2
                            }
                        }
                        if (match) for (j in aValue.indices) for (i in aValue[j].indices) {
                            if (bValue[j][i] && x + i in 0 until it.width && y + j in 0 until it.height) {
                                setRGB(x + i, y + j, WHITE)
                            }
                        }
                    }
                }
            }
        }
    }
