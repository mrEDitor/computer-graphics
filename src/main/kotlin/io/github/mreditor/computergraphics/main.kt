package io.github.mreditor.computergraphics

import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import java.awt.*
import java.awt.datatransfer.DataFlavor
import java.awt.datatransfer.Transferable
import java.awt.event.ActionEvent
import java.awt.event.ComponentAdapter
import java.awt.event.ComponentEvent
import java.awt.event.KeyEvent
import java.awt.image.BufferedImage
import java.io.File
import java.lang.Integer.max
import java.lang.Integer.min
import java.util.*
import javax.imageio.ImageIO
import javax.swing.*
import javax.swing.border.BevelBorder
import kotlin.concurrent.thread
import kotlin.system.measureTimeMillis

fun main() {
    MainWindow.isVisible = true
}

const val WHITE = 0xFFFFFF
const val BLACK = 0x000000

object MainWindow : JFrame("Computer graphics") {
    var sourceImage
        get() = sourceImages.lastOrNull()
        set(value) {
            value?.let { sourceImages.addLast(it) }
            if (sourceImages.size > 10) sourceImages.removeFirst()
            preview()
        }
    private var sourceImages = ArrayDeque<BufferedImage>()
    private val previewContainer = JLabel()
    private val resizePreviews = JCheckBoxMenuItem("Fit image to window (slower)", true)
    private val menu = JMenuBar().apply {
        add(JMenu("File").apply {
            add(resizePreviews.apply {
                addActionListener { preview() }
            })
            addJMenuItem("Undo", KeyStroke.getKeyStroke(KeyEvent.VK_Z, ActionEvent.CTRL_MASK)) { undo() }
            addJMenuItem("Open", KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK)) { openImage() }
            addJMenuItem("Paste", KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK)) { pasteImage() }
            addJMenuItem("Exit") { this@MainWindow.isVisible = false }
        })
        add(JMenu("Filters").apply {
            addJMenuItem("Downscale") { downscaleFilter() }
            addJMenuItem("Grayscale") { grayscaleFilter() }
            addJMenuItem("Black and white") { blackAndWhiteFilter() }
            addJMenuItem("Median filter (filter out salt)") { medianFilter() }
            addJMenuItem("Gradient magnitude (Sobel operators)") { sobelGradientMagnitude() }
            addJMenuItem("Gradient orientation (Sobel operators)") { sobelGradientOrientation() }
        })
        add(JMenu("Count").apply {
            addJMenuItem("Breadth-first search") { bfsCount() }
            addJMenuItem("ABC mask") { maskCount() }
        })
        add(JMenu("Fill").apply {
            addJMenuItem("Breadth-first search") { bfsFill() }
            addJMenuItem("ABC mask") { maskFill() }
            addJMenuItem("ABC mask (left-to-right, skip dsu)") { noDsuMaskFill(true) }
            addJMenuItem("ABC mask (top-to-bottom, skip dsu)") { noDsuMaskFill(false) }
        })
        add(JMenu("Tools").apply {
            addJMenuItem("Morphology (b/w)") { showMorphologyTool() }
            add(JMenu("Convolution").apply {
                addJMenuItem("Identity preset") {
                    showConvolutionTool()
                }
                addJMenuItem("Gaussian blur 7*7 preset") {
                    showConvolutionTool("Gauss", GAUSSIAN_7)
                }
                addJMenuItem("Sobel-X operator 3*3 preset") {
                    showConvolutionTool("Sobel X'", SOBELX_3)
                }
                addJMenuItem("Sobel-Y operator 3*3 preset") {
                    showConvolutionTool("Sobel Y'", SOBELY_3)
                }
            })
            addJMenuItem("Histogram") { Histogram.isVisible = true }
        })
    }

    private val statusLabel = JLabel("ready").apply {
        horizontalAlignment = SwingConstants.LEFT
    }

    private val statusPanel = JPanel().apply {
        border = BevelBorder(BevelBorder.LOWERED)
        preferredSize = Dimension(Int.MAX_VALUE, 16)
        layout = BoxLayout(this, BoxLayout.X_AXIS)
        add(statusLabel)
    }

    init {
        defaultCloseOperation = EXIT_ON_CLOSE
        bounds = Rectangle(80, 80, 480, 480)
        isResizable = true
        transferHandler = object : TransferHandler() {
            override fun canImport(support: TransferSupport): Boolean =
                support.isDataFlavorSupported(DataFlavor.imageFlavor)
                        || support.isDataFlavorSupported(DataFlavor.javaFileListFlavor)

            override fun importData(support: TransferSupport): Boolean =
                canImport(support) && importTransferable(support.transferable)
        }
        add(previewContainer)
        add(statusPanel, BorderLayout.SOUTH)
        add(menu, BorderLayout.NORTH)
        addComponentListener(object : ComponentAdapter() {
            override fun componentResized(e: ComponentEvent?) = preview(false)
        })
    }

    private fun importTransferable(transferable: Transferable) = when {
        transferable.isDataFlavorSupported(DataFlavor.imageFlavor) ->
            importImage(transferable.getTransferData(DataFlavor.imageFlavor))
        transferable.isDataFlavorSupported(DataFlavor.javaFileListFlavor) ->
            importFile(transferable.getTransferData(DataFlavor.javaFileListFlavor))
        else ->
            false
    }


    private fun importFile(transferData: Any): Boolean {
        if (transferData is ArrayList<*>) {
            (transferData[0] as? File)?.let {
                importFile(it)
                return true
            }
        }
        return false
    }

    private fun importImage(transferData: Any): Boolean {
        (transferData as? Image)?.let {
            val bufferedImage = BufferedImage(it.getWidth(null), it.getHeight(null), BufferedImage.TYPE_INT_RGB)
            val graphics2D = bufferedImage.createGraphics()
            graphics2D.drawImage(it, 0, 0, null)
            graphics2D.dispose()
            importImage(bufferedImage)
            return true
        }
        return false
    }

    private fun undo() {
        if (sourceImages.size > 1) {
            sourceImages.removeLast()
        }
        preview(false)
    }

    private fun importImage(image: BufferedImage) {
        sourceImage = image
    }

    private fun importFile(it: File) {
        importImage(
            withStatus("reading file " + it.absolutePath) {
                ImageIO.read(it)
            }
        )
    }

    private fun openImage() {
        JFileChooser().apply {
            if (showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                importFile(selectedFile)
            }
        }
    }

    private fun pasteImage() {
        val transferable = Toolkit.getDefaultToolkit().systemClipboard.getContents(null)
        transferable?.let { importTransferable(it) }
    }

    private fun JMenu.addJMenuItem(label: String, accel: KeyStroke? = null, actionListener: () -> Unit) =
        add(JMenuItem(label, mnemonic).apply {
            accel?.let { accelerator = it }
            addActionListener { actionListener() }
        })

    private val mutex = Mutex()
    private fun preview(vital: Boolean = true) {
        sourceImage?.let {
            if (!mutex.tryLock()) {
                if (vital) {
                    runBlocking { mutex.lock() }
                } else {
                    return
                }
            }
            withStatus("resizing preview", onExit = { mutex.unlock() }) {
                previewContainer.icon = ImageIcon(
                    if (resizePreviews.isSelected)
                        it.getScaledInstance(
                            contentPane.width,
                            contentPane.height - menu.height - statusPanel.height,
                            Image.SCALE_FAST
                        )
                    else it
                )
                Histogram.refresh()
            }
        }
    }

    fun withStatus(
        status: String,
        operationName: ((ms: Long) -> String)? = null,
        onExit: (() -> Unit)? = null,
        action: () -> Unit
    ) {
        val oldStatus = statusLabel.text
        statusLabel.text = status
        val task = {
            var time = 0L
            try {
                time = measureTimeMillis(action)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            SwingUtilities.invokeAndWait {
                statusLabel.text = operationName?.invoke(time) ?: oldStatus
                onExit?.invoke()
            }
        }
        if (SwingUtilities.isEventDispatchThread()) {
            thread(block = task)
        } else {
            task()
        }
    }

    fun withStatus(status: String, operationName: String, action: () -> Unit) =
        withStatus(status, operationName = { "$operationName in $it ms" }) { action() }

    fun runWithStatus(status: String, operationName: String? = null, action: MainWindow.() -> Unit) =
        withStatus(status, operationName?.let { { t: Long -> "$operationName in $t ms" } }) { MainWindow.action() }
}

fun Int.red() = (this shr 16) and 0xFF
fun Int.green() = (this shr 8) and 0xFF
fun Int.blue() = this and 0xFF
fun rgb(r: Int, g: Int, b: Int) =
    (max(0, min(255, r)) shl 16) or (max(0, min(255, g)) shl 8) or max(0, min(255, b))