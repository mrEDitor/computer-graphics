package io.github.mreditor.computergraphics

import org.jfree.chart.ChartFactory
import org.jfree.chart.ChartPanel
import org.jfree.chart.axis.NumberAxis
import org.jfree.chart.axis.NumberTickUnit
import org.jfree.data.Range
import org.jfree.data.statistics.HistogramDataset
import java.awt.image.BufferedImage
import java.awt.image.PixelGrabber
import java.lang.Integer.max
import java.lang.Integer.min
import javax.swing.*
import kotlin.math.roundToInt

private const val S = 1
private const val T = S / 3.0

object Histogram : JFrame("RGB histogram") {
    private val histogramHolder = JPanel()
    private val redCheckBox = JCheckBox("Red").apply {
        isSelected = true
        addActionListener { refresh() }
    }
    private val greenCheckBox = JCheckBox("Green").apply {
        isSelected = true
        addActionListener { refresh() }
    }
    private val blueCheckBox = JCheckBox("Blue").apply {
        isSelected = true
        addActionListener { refresh() }
    }

    init {
        add(Box.createVerticalBox().apply {
            add(histogramHolder)
            add(Box.createHorizontalBox().apply {
                add(redCheckBox)
                add(greenCheckBox)
                add(blueCheckBox)
                add(JButton("Normalize").apply {
                    addActionListener { recolor(::recolorLinear) }
                })
                add(JButton("Equalize").apply {
                    addActionListener {
                        recolor { src, pixels, red, green, blue ->
                            recolorEqualize(src, pixels, red, green, blue, false)
                        }
                    }
                })
            })
        })
    }

    fun refresh(): Unit = MainWindow.withStatus("Refreshing histogram") {
        MainWindow.sourceImage?.apply {
            val pixels = IntArray(width * height).also {
                PixelGrabber(this, 0, 0, width, height, it, 0, width).grabPixels()
            }
            val data = HistogramDataset().apply {
                fun collect(b: Boolean, i: Int, s: Int) =
                    if (b) pixels.map { i * T + ((it shr s) and 0xFF) * S }.toDoubleArray()
                    else DoubleArray(1) { 0.0 }
                addSeries("Red", collect(redCheckBox.isSelected, 1, 8), S * 256)
                addSeries("Blue", collect(blueCheckBox.isSelected, 2, 0), S * 256)
                addSeries("Green", collect(greenCheckBox.isSelected, 1, 16), S * 256)
            }
            histogramHolder.apply {
                removeAll()
                add(
                    ChartPanel(
                        ChartFactory
                            .createHistogram("RGB histogram", "Color", "Count", data)
                            .apply {
                                (xyPlot.domainAxis as NumberAxis).apply {
                                    tickUnit = NumberTickUnit(16.0)
                                    range = Range(0.0, 256.0)
                                }
                            },
                        600, 600, 600, 600, 600, 600,
                        true, true, true, true, true, true
                    )
                )
            }
            pack()
            repaint()
        }
    }
}

private fun recolor(action: (BufferedImage, IntArray, IntArray, IntArray, IntArray) -> BufferedImage) =
    MainWindow.runWithStatus("Collecting data from histogram") {
        val pixels = sourceImage
            ?.let {
                IntArray(it.width * it.height).apply {
                    PixelGrabber(it, 0, 0, it.width, it.height, this, 0, it.width).grabPixels()
                }
            }
            ?: IntArray(0)
        val red = IntArray(256)
        val green = IntArray(256)
        val blue = IntArray(256)
        for (p in pixels) {
            red[p.red()] = red[p.red()] + 1
            green[p.green()] = green[p.green()] + 1
            blue[p.blue()] = blue[p.blue()] + 1
        }
        sourceImage = sourceImage?.let {
            action(it, pixels, red, green, blue)
        }
    }

private fun recolorLinear(
    src: BufferedImage,
    pixels: IntArray,
    red: IntArray,
    green: IntArray,
    blue: IntArray
): BufferedImage {
    val d = JOptionPane
        .showInputDialog(MainWindow, "Color loose threshold", "0.001")
        .toDouble()
    val t = src.width * src.height * d
    val minBlue = blue.indexOfFirst { it > t }
    val minGreen = green.indexOfFirst { it > t }
    val minRed = red.indexOfFirst { it > t }
    val maxBlue = blue.indexOfLast { it > t }
    val maxGreen = green.indexOfLast { it > t }
    val maxRed = red.indexOfLast { it > t }
    return BufferedImage(src.width, src.height, BufferedImage.TYPE_INT_RGB).apply {
        for (i in pixels.indices) {
            val color = rgb(
                normalize(pixels[i].red(), minRed, maxRed),
                normalize(pixels[i].green(), minGreen, maxGreen),
                normalize(pixels[i].blue(), minBlue, maxBlue)
            )
            setRGB(i % src.width, i / src.width, color)
        }
    }
}

private fun recolorEqualize(
    src: BufferedImage,
    pixels: IntArray,
    red: IntArray,
    green: IntArray,
    blue: IntArray,
    fixPeaks: Boolean
): BufferedImage {
    val sumRed = red.copyOf()
    val sumGreen = green.copyOf()
    val sumBlue = blue.copyOf()
    val sumEq = DoubleArray(256)
    for (i in 1..255) {
        sumRed[i] += sumRed[i - 1]
        sumGreen[i] += sumGreen[i - 1]
        sumBlue[i] += sumBlue[i - 1]
        sumEq[i] = pixels.size / 256.0 * i
    }
    val mapRed = IntArray(256)
    val mapGreen = IntArray(256)
    val mapBlue = IntArray(256)
    val redAfterMap = IntArray(256)
    val greenAfterMap = IntArray(256)
    val blueAfterMap = IntArray(256)
    var iterRed = 0
    var iterGreen = 0
    var iterBlue = 0
    for (i in 0..255) {
        // todo: binary search is applicable
        // todo sumRed/Green/Blue aren't necessary
        val t = pixels.size / 256
        fun updateIter(i: Int, a: IntArray, c: Int): Int {
            return if (fixPeaks && i < 255 && (a[i] > t || a[i] + c > t)) {
                i + 1
            } else {
                i
            }.also { a[it] += c }
        }
        iterRed = updateIter(iterRed, redAfterMap, red[i])
        mapRed[i] = iterRed
        while (iterRed < 255 && sumEq[iterRed + 1] <= sumRed[i]) {
            redAfterMap[iterRed] -= red[iterRed]
            iterRed = updateIter(++iterRed, redAfterMap, red[i])
            mapRed[i] = iterRed
        }
        iterGreen = updateIter(iterGreen, greenAfterMap, green[i])
        mapGreen[i] = iterGreen
        while (iterGreen < 255 && sumEq[iterGreen + 1] <= sumGreen[i]) {
            greenAfterMap[iterGreen] -= green[iterGreen]
            iterGreen = updateIter(++iterGreen, greenAfterMap, green[i])
            mapGreen[i] = iterGreen
        }
        iterBlue = updateIter(iterBlue, blueAfterMap, blue[i])
        mapBlue[i] = iterBlue
        while (iterBlue < 255 && sumEq[iterBlue + 1] <= sumBlue[i]) {
            blueAfterMap[iterBlue] -= blue[iterBlue]
            iterBlue = updateIter(++iterBlue, blueAfterMap, blue[i])
            mapBlue[i] = iterBlue
        }
    }
    return BufferedImage(src.width, src.height, BufferedImage.TYPE_INT_RGB).apply {
        for (x in 0 until src.width) for (y in 0 until src.height) {
            val c = src.getRGB(x, y)
            setRGB(
                x, y, rgb(
                    mapRed[c.red()],
                    mapGreen[c.green()],
                    mapBlue[c.blue()]
                )
            )
        }
    }
}

private fun normalize(color: Int, min: Int, max: Int): Int {
    val v = (color - min) / max(1, max - min).toDouble()
    return max(0x00, min(0xFF, (v * 256).roundToInt()))
}
