package io.github.mreditor.computergraphics

import java.awt.Graphics2D
import java.awt.GridLayout
import java.awt.image.BufferedImage
import java.lang.Integer.min
import javax.swing.JLabel
import javax.swing.JOptionPane
import javax.swing.JPanel
import javax.swing.JTextField
import kotlin.math.min
import kotlin.system.measureTimeMillis

fun downscaleFilter(): Unit = MainWindow.run {
    withStatus("downscaling") {
        sourceImage = sourceImage?.let {
            val w = JTextField(it.width.toString())
            val h = JTextField(it.height.toString())
            if (JOptionPane.showConfirmDialog(this, JPanel(GridLayout(0, 1)).apply {
                    add(JLabel("Width:"))
                    add(w)
                    add(JLabel("Height:"))
                    add(h)
                }, "Resize", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.CANCEL_OPTION) null
            else BufferedImage(w.text.toInt(), h.text.toInt(), BufferedImage.TYPE_INT_RGB).apply {
                (graphics as Graphics2D).apply {
                    scale(w.text.toDouble() / it.width, h.text.toDouble() / it.height)
                    drawImage(it, 0, 0, null)
                    dispose()
                }
            }
        }
    }
}

fun grayscaleFilter(): Unit = MainWindow.runWithStatus("filtering to grayscale", "grayscaled") {
    sourceImage?.let {
        val r = JTextField("0.3")
        val g = JTextField("0.59")
        val b = JTextField("0.11")
        if (JOptionPane.showConfirmDialog(this, JPanel(GridLayout(0, 1)).apply {
                add(JLabel("Red:"))
                add(r)
                add(JLabel("Green:"))
                add(g)
                add(JLabel("Blue:"))
                add(b)
            }, "Greyscale", JOptionPane.OK_CANCEL_OPTION) != JOptionPane.CANCEL_OPTION) {
            sourceImage = BufferedImage(it.width, it.height, BufferedImage.TYPE_INT_RGB).apply {
                for (x in 0 until it.width) for (y in 0 until it.height) {
                    setRGB(
                        x,
                        y,
                        it.getRGB(x, y).grayscale(r.text.toDouble(), g.text.toDouble(), b.text.toDouble())
                    )
                }
            }
        }
    }
}

fun blackAndWhiteFilter(): Unit = MainWindow.run {
    withStatus("filtering to black and white") {
        sourceImage?.let {
            val r = JTextField("0.3")
            val g = JTextField("0.59")
            val b = JTextField("0.11")
            val w = JTextField("0.5")
            if (JOptionPane.showConfirmDialog(this, JPanel(GridLayout(0, 1)).apply {
                    add(JLabel("Red:"))
                    add(r)
                    add(JLabel("Green:"))
                    add(g)
                    add(JLabel("Blue:"))
                    add(b)
                    add(JLabel("White threshold:"))
                    add(w)
                }, "Black and white", JOptionPane.OK_CANCEL_OPTION) != JOptionPane.CANCEL_OPTION) {
                sourceImage = BufferedImage(it.width, it.height, BufferedImage.TYPE_INT_RGB).apply {
                    val wt = (w.text.toDouble() * 0xFF).toInt()
                    for (x in 0 until it.width) for (y in 0 until it.height) {
                        setRGB(
                            x, y, it.getRGB(x, y).grayscale(
                                r.text.toDouble(),
                                g.text.toDouble(),
                                b.text.toDouble()
                            ).monochrome(wt)
                        )
                    }
                }
            }
        }
    }
}

fun Int.grayscale(r: Double, g: Double, b: Double): Int {
    val grayscale = min(255.0, r * this.red() + g * this.green() + b * this.blue()).toInt()
    return rgb(grayscale, grayscale, grayscale)
}

fun Int.monochrome(t: Int) =
    if ((this and 0xFF) < t) BLACK else WHITE
