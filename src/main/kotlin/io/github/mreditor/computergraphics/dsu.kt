package io.github.mreditor.computergraphics

class Dsu {
    private val parent = mutableMapOf<Int, Int>()
    private val rank = mutableMapOf<Int, Int>()
    val size get() = parent.size

    operator fun get(v: Int): Int {
        if (v == 0) throw IllegalArgumentException()
        parent[v]?.let {
            if (v != it) {
                parent[v] = get(it)
                return parent[v]!!
            }
        }
        return v
    }

    operator fun set(u: Int, v: Int) {
        if (u or v == 0) throw IllegalArgumentException()
        val a = get(u)
        val b = get(v)
        if (a != b) {
            when (rank.getOrDefault(a, 0) compareTo rank.getOrDefault(b, 0)) {
                -1 -> parent[b] = a
                -0 -> {
                    parent[b] = a
                    rank[a] = rank.getOrDefault(a, 0) + 1
                }
                +1 -> parent[a] = b
            }
        }
    }

    private infix fun Int.compareTo(another: Int) = compareTo(another)
}
